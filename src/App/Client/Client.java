package App.Client;

import App.Parser.ParserXML;
import App.Server.ServerA;


import java.io.IOException;
import java.net.*;
import java.nio.file.Paths;
import java.util.*;


public class Client {

    public static final int PORT = 4445;

    private DatagramSocket ds;
    private InetAddress serverHost;

    private String codificacion;

    public Client() {
        try {
            this.ds = new DatagramSocket(PORT);
            this.serverHost = InetAddress.getByName(listAllBroadcastAddresses().get(0).getCanonicalHostName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DatagramSocket getDs() {
        return ds;
    }

    public InetAddress getServerHost() {
        return serverHost;
    }

    public String comandoCliente() {
        return "COMANDOS A UTILIZAR\n" +
                "\tINICIAR consultas de datos al broadcast [START]\n" +
                "\tDETENER consultas de datos al broadcast [STOP]\n" +
                "\tHELP interactuar con los servidores [HELP]\n" +
                "\tSERIELIZADOR de los servidores [JSON/XML]\n" +
                "\tSalir de la aplicación [EXIT]\n" +
                "> ";
    }

    public String imprimirHelp() {
        Scanner scanner = new Scanner(System.in);
        System.out.print(comandoCliente());
        return scanner.nextLine().toLowerCase();
    }

    // TODO Ver aquí si lo mando como un JSON o un XML.
    public void enviarPeticion(String peticion, int port) {
        ParserXML parserXML = new ParserXML();
        String datosRecibidos;
        String respuestaSplit[];
        if (this.codificacion.equals("xml")) {
            try {
                // Enviamos la peticion al servidor
                // Generamos los datos de control para ser enviados por parte del Cliente
                String datos = parserXML.serializeXMLControl(peticion);
                parserXML.validateXML(datos, Paths.get("").toAbsolutePath().toString() + "/src/App/DataBase/DatosControl.xsd");
                byte[] buffer = datos.getBytes();
                DatagramSocket datagramSocket = new DatagramSocket();
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, this.serverHost, port);
                datagramSocket.send(packet);

                // Recibimos la peticion del servidor.
                byte[] bufer = new byte[ServerA.MAX_UDP_PACKET_LENGTH];
                DatagramPacket respuesta = new DatagramPacket(bufer, bufer.length);
                datagramSocket.receive(respuesta);
                datosRecibidos = new String(respuesta.getData());
                respuestaSplit = datosRecibidos.split("SERVER: ");
                respuestaSplit = respuestaSplit[1].split("\r\n\r\n");

                System.out.println(respuestaSplit[0]);
                if (respuestaSplit[0].contains("ServerA") && !respuestaSplit[0].contains("control")) {
                    respuestaSplit = respuestaSplit[0].split("ServerA : ");
                    parserXML.validateXML(respuestaSplit[1], Paths.get("").toAbsolutePath().toString() + "/src/App/DataBase/DatosDifusionServerA.xsd");


                } else if (respuestaSplit[0].contains("ServerB") && respuestaSplit[0].contains("control")) {
                    respuestaSplit = respuestaSplit[0].split("ServerB : ");
                    parserXML.validateXML(respuestaSplit[1], Paths.get("").toAbsolutePath().toString() + "/src/App/DataBase/DatosDifusionServerB.xsd");

                } else if (respuestaSplit[0].contains("control")) {
                    if(respuestaSplit[0].contains("ServerA"))
                        respuestaSplit = respuestaSplit[0].split("ServerA : ");
                    else
                        respuestaSplit = respuestaSplit[0].split("ServerB : ");
                    parserXML.validateXML(respuestaSplit[1], Paths.get("").toAbsolutePath().toString() + "/src/App/DataBase/DatosControl.xsd");
                }
                System.out.println("XML:\n" + respuestaSplit[1]);

            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (this.codificacion.equals("json")) {
            try {
                // Generamos los datos con la codificacion JSON
                // Enviamos los datos
                String datos = parserXML.serializeJSONServerA(peticion);
                byte[] buffer = datos.getBytes();
                DatagramSocket datagramSocket = new DatagramSocket();
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, this.serverHost, port);
                datagramSocket.send(packet);

                // Recibimos la peticion del servidor(NO HACE FALTA VALIDAR)
                byte[] bufer = new byte[ServerA.MAX_UDP_PACKET_LENGTH];
                DatagramPacket respuesta = new DatagramPacket(bufer, bufer.length);
                datagramSocket.receive(respuesta);
                datosRecibidos = new String(respuesta.getData());
                respuestaSplit = datosRecibidos.split("JSON: ");
                respuestaSplit = respuestaSplit[1].split("\r\n\r\n");
                System.out.println("JSON:\n" + parserXML.deserializeJSONServerA(respuestaSplit[0]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void establecerCodificacion(String codificacion) {
        try {
            this.codificacion = codificacion;
            byte[] buffer = codificacion.getBytes();
            DatagramSocket datagramSocket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, this.serverHost, ServerA.PORT);
            datagramSocket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int elegirServer() {
        System.out.print("Elige un servidor para interacturar: [A | B]" +
                "\n> ");
        String opServer = new Scanner(System.in).nextLine().toLowerCase();
        if (opServer.equalsIgnoreCase("A"))
            return ServerA.PORT;
        else if (opServer.equalsIgnoreCase("B"))
            return 1;
        return 0;

    }

    private List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastList = new ArrayList<>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();
            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }
            networkInterface.getInterfaceAddresses().stream().map(InterfaceAddress::getBroadcast).filter(Objects::nonNull).forEach(broadcastList::add);
        }
        return broadcastList;
    }

    private void mainCliente() {
        System.out.println("INICIO DE SERVIDOR");
        ClientThread ct = null;
        String codificacion = " ";

        // Establecer codificacion
        while (!codificacion.equalsIgnoreCase("JSON") && !codificacion.equalsIgnoreCase("XML")) {
            System.out.println("FORMA DE CODIFICAR LOS DATOS:\n\t[JSON]\n\t[XML]");
            codificacion = new Scanner(System.in).nextLine().toLowerCase();
        }
        this.codificacion = codificacion;
        System.out.println("Codificacion seleccionada: " + codificacion);
        this.establecerCodificacion(codificacion);

        String op = this.imprimirHelp();
        while (!op.equals("exit")) {
            switch (op) {

                // Cada hilo realiza la petición al servidor.
                case "start":
                    (ct = new ClientThread(this, this.getDs(), this.getServerHost(), op, this.codificacion)).start();
                    op = this.imprimirHelp();
                    break;

                case "stop":
                    if (ct != null) {
                        ct.interrupt();
                        op = this.imprimirHelp();
                        ct = null;
                    } else {
                        System.out.println("No se ha iniciado ninguna peticion");
                        op = this.imprimirHelp();
                    }
                    break;

                // Cambio de codificacion
                case "json":
                    this.establecerCodificacion("json");
                    op = this.imprimirHelp();
                    break;

                case "xml":
                    this.establecerCodificacion("xml");
                    op = this.imprimirHelp();
                    break;

                // Comando para ayuda
                case "help":

                    // Elegir el servidor para interactuar.
                    int opServer;
                    if ((opServer = this.elegirServer()) == 0) {
                        System.out.println("Servidor no disponible");
                        break;
                    }
                    // Envio la peticion al servidor
                    this.enviarPeticion(op, opServer);
                    // Entramos en un nuevo shell para el cliente con los comandos del servidor
                    while (!op.equals("exit")) {
                        op = new Scanner(System.in).nextLine().toLowerCase();
                        this.enviarPeticion(op, opServer);
                        System.out.print("> ");
                    }
                    op = this.imprimirHelp();
                    break;
                case "exit":
                    return;
                default:
                    System.out.println("Comando no valido");
                    op = this.imprimirHelp();
                    break;
            }
        }

        System.out.println("FIN DE SERVIDOR");


    }


    public static void main(String[] args) {

        Client client = new Client();
        client.mainCliente();
    }


}
