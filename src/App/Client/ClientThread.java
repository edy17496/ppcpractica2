package App.Client;

import App.Server.Server;
import App.Server.ServerA;

import javax.swing.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class ClientThread extends Thread {

    private DatagramSocket ds;
    private InetAddress serverHost;
    private int serverPort;
    private String peticion;
    private Client cliente;

    public ClientThread(Client cliente, DatagramSocket ds, InetAddress serverHost, String peticion, String codificacion) {
        this.ds = ds;
        this.serverHost = serverHost;
        this.peticion = peticion;
        this.cliente = cliente;
    }


    public void run() {
        // Pasamos por parametro el mensaje que deseamos enviar
        while (true) {
            while (true) {
                try {
                    Thread.sleep(2500);
                    this.cliente.enviarPeticion(peticion, ServerA.PORT);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public void imprimirResuesta(String respuesta) {
        String[] parts = respuesta.split("\n");
        for (int i = 0; i < 4; i++) {
            System.out.println(parts[i]);
        }
    }
}
