package App.Server;

import App.Client.Client;
import App.Parser.ParserXML;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

public abstract class Server {

    // TODO Tengo que leer el tamaño del mensaje, no mas porque me lee zupia
    public boolean recibirMensaje(DatagramSocket socket, String codificacion) {
        ParserXML parserXML = new ParserXML();
        byte[] buffer = new byte[ServerA.MAX_UDP_PACKET_LENGTH];
        String[] parts;
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        boolean validacion = true;

        // TODO Aquí tenemos que deserealizar segun la codificacion
        try {
            // Recibir mensajes por parte del cliente
            socket.setSoTimeout(1000);
            socket.receive(packet);
            parts = new String(packet.getData()).split("\r\n\r\n");
            System.out.println("RECEIVE: " + parts[0]);

            if (!parts[0].contains("xml") && !parts[0].contains("json"))
                if (codificacion.equals("xml"))
                    validacion = parserXML.validateXML(parts[0], Paths.get("").toAbsolutePath().toString() + "/src/App/DataBase/DatosControl.xsd");
            tratamientoMensaje(parts[0], socket, packet);
        } catch (IOException e) {
            System.err.println("TimeOut: Not data input");
            return false;
        }
        return validacion;
    }

    public String establecerCodificacion(DatagramSocket socket) {
        String codificacion;
        byte[] buffer = new byte[ServerA.MAX_UDP_PACKET_LENGTH];
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        try {
            // Recibir mensajes por parte del cliente
            socket.receive(packet);
            codificacion = new String(packet.getData()).toLowerCase();
            System.out.println("CODIFICACION: " + codificacion);
            if (codificacion.contains("xml"))
                return "xml";
            else if (codificacion.contains("json"))
                return "json";
        } catch (IOException e) {
            System.err.println("Invalid serialized");
            return null;
        }
        return null;
    }

    public boolean enviarMensaje(String mensaje) {
        try {
            Thread.sleep(2500);
            byte[] buffer = mensaje.getBytes();
            DatagramSocket datagramSocket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(listAllBroadcastAddresses().get(0).getCanonicalHostName()), Client.PORT);
            datagramSocket.send(packet);
            System.out.println("SEND: " + new String(packet.getData()));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void enviarMensaje(String mensaje, DatagramSocket socket, DatagramPacket packet,String codificacion) {
        ParserXML parserXML = new ParserXML();
        try {
            byte[] response = mensaje.getBytes();
            DatagramPacket respuesta = new DatagramPacket(response, response.length, packet.getAddress(), packet.getPort());
            socket.send(respuesta);
            System.out.println("SEND: " + new String(respuesta.getData()) + " " + respuesta.getAddress() + " " + respuesta.getPort());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String HelpServer() {
        return "COMANDOS\n";
    }

    private List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastList = new ArrayList<>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();
            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }
            networkInterface.getInterfaceAddresses().stream().map(InterfaceAddress::getBroadcast).filter(Objects::nonNull).forEach(broadcastList::add);
        }
        return broadcastList;
    }

    public String crearDatos(String server, String codificacion, String xml) {
        return "SERVER: " + server + " " + ": " + xml;
    }

    abstract protected void tratamientoMensaje(String mensaje, DatagramSocket socket, DatagramPacket packet);

}
