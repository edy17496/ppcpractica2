package App.Server;

import App.Parser.ParserXML;

import java.net.*;
import java.nio.file.Paths;

public class ServerA extends Server {

    public transient static final int MAX_UDP_PACKET_LENGTH = 1450;
    public transient static final int PORT = 4446;

    private transient DatagramSocket ds;
    private transient String datosEnviar;
    private transient String medidaTempertatura;

    // Tipo de codificacion
    private transient String codificacion;

    // Atributos para la serializacion JSON.
    private StringBuilder humedad;
    private StringBuilder temperatura_celcius;
    private StringBuilder temperatura_kelvin;
    private StringBuilder pluviometria;

    public ServerA() {
        try {
            this.ds = new DatagramSocket(PORT);
            this.medidaTempertatura = "celc";
        } catch (SocketException e) {
            System.err.println("No se ha podido crear el socket.");
        }
    }

    public ServerA(String medidaTempertatura) {
        this.medidaTempertatura = medidaTempertatura;
        this.humedad = new StringBuilder();
        switch (medidaTempertatura) {
            case "celc":
                this.temperatura_celcius = new StringBuilder();
                break;

            case "kelv":
                this.temperatura_kelvin = new StringBuilder();
                break;
        }
        this.pluviometria = new StringBuilder();
    }

    public void setDatos() {
        StringBuilder humedad = new StringBuilder();
        StringBuilder temperatura_celcius;
        StringBuilder temperatura_kelvin;
        StringBuilder pluviometria = new StringBuilder();
        switch (this.medidaTempertatura) {
            case "celc":
                temperatura_celcius = new StringBuilder();
                for (int i = 1; i <= 10; i++) {
                    humedad.append((int) (Math.random() * 100 + 1)).append(" ");
                    temperatura_celcius.append((int) (Math.random() * 50 + 1)).append(" ");
                    pluviometria.append((int) (Math.random() * 100 + 1)).append(" ");

                }
                this.setTemperatura_celcius(temperatura_celcius);
                break;
            case "kelv":
                temperatura_kelvin = new StringBuilder();
                for (int i = 1; i <= 10; i++) {
                    humedad.append((int) (Math.random() * 100 + 1)).append(" ");
                    temperatura_kelvin.append((int) (Math.random() * 50 + 1)).append(" ");
                    pluviometria.append((int) (Math.random() * 100 + 1)).append(" ");
                }
                this.setTemperatura_kelvin(temperatura_kelvin);
                break;
        }
        this.setHumedad(humedad);
        this.setPluviometria(pluviometria);
    }

    public String getMedidaTempertatura() {
        return medidaTempertatura;
    }

    public void setMedidaTempertatura(String medidaTempertatura) {
        this.medidaTempertatura = medidaTempertatura;
    }

    public StringBuilder getHumedad() {
        return humedad;
    }

    public void setHumedad(StringBuilder humedad) {
        this.humedad = humedad;
    }

    public StringBuilder getTemperatura_celcius() {
        return temperatura_celcius;
    }

    public void setTemperatura_celcius(StringBuilder temperatura_celcius) {
        this.temperatura_celcius = temperatura_celcius;
    }

    public StringBuilder getTemperatura_kelvin() {
        return temperatura_kelvin;
    }

    public void setTemperatura_kelvin(StringBuilder temperatura_kelvin) {
        this.temperatura_kelvin = temperatura_kelvin;
    }

    public StringBuilder getPluviometria() {
        return pluviometria;
    }

    public void setPluviometria(StringBuilder pluviometria) {
        this.pluviometria = pluviometria;
    }

    private DatagramSocket getDs() {
        return this.ds;
    }

    public String toString() {
        switch (this.medidaTempertatura) {
            case "celc":
                return "ServerA {" +
                        "humedad=" + humedad +
                        ", temperatura_celcius=" + temperatura_celcius +
                        ", pluviometria=" + pluviometria +
                        '}';
            case "kelv":
                return "ServerA {" +
                        "humedad=" + humedad +
                        ", temperatura_kelvin=" + temperatura_kelvin +
                        ", pluviometria=" + pluviometria +
                        '}';
        }
        return null;
    }

    public String crearDatos(String medidadaTemperatura) {
        ParserXML parserXML = new ParserXML();
        String datos;
        switch (this.codificacion) {
            case "xml":
                datos = parserXML.serializeXMLServerA(medidadaTemperatura);
                parserXML.validateXML(datos, Paths.get("").toAbsolutePath().toString() + "/src/App/DataBase/DatosDifusionServerA.xsd");
                return super.crearDatos(this.getClass().getSimpleName(), this.codificacion, datos);
            case "json":
                datos = parserXML.serializeJSONServerA(medidadaTemperatura);
                return super.crearDatos(this.getClass().getSimpleName(), this.codificacion, datos);
        }
        return null;
    }

    public String helpServerA() {
        return super.HelpServer() +
                "\tCAMBIAR MEDIDA temperatura a Celcius [CELC]\n" +
                "\tCAMBIAR MEDIDA temperatura a Kelvin [KELV]\n" +
                "\tSALIR de los servicios del servidor [EXIT]\n";
    }


    @Override
    public void tratamientoMensaje(String mensaje, DatagramSocket socket, DatagramPacket packet) {
        ParserXML parserXML = new ParserXML();
        String op = " ";
        if (mensaje.contains("xml") && !mensaje.contains("control"))
            op = "xml";
        else if (mensaje.contains("json") && !mensaje.contains("control"))
            op = "json";
        else op = parserXML.getComandoXML(mensaje);

        System.out.println("OP:" + op);
        op = op.toLowerCase();
        switch (op) {
            case "help":
                String datosEnviarHelp = parserXML.serializeXMLControl(helpServerA());
                System.out.println("SA:" + datosEnviarHelp);
                super.enviarMensaje(crearDatos(this.getClass().getSimpleName(), this.codificacion, datosEnviarHelp), socket, packet, this.codificacion);
                break;
                // TODO Arreglar porque se esta enviando mal los datos.
            case "kelv":
                // Creo los datos con las nuevas medidas
                this.medidaTempertatura = op;
                this.datosEnviar = crearDatos(this.codificacion, this.codificacion, this.medidaTempertatura);
                enviarMensaje(this.datosEnviar, socket, packet, this.codificacion);
                break;
            case "celc":
                // Creo los datos con las nuevas medidas
                this.medidaTempertatura = op;
                this.datosEnviar = crearDatos(this.codificacion, this.codificacion, this.medidaTempertatura);
                enviarMensaje(this.datosEnviar, socket, packet, this.codificacion);
                break;
            case "exit":
                enviarMensaje(crearDatos(this.getClass().getSimpleName(), this.codificacion, parserXML.serializeXMLControl(op)), socket, packet, this.codificacion);
                break;
            case "json":
                this.codificacion = op;
                this.datosEnviar = crearDatos(this.medidaTempertatura);
                enviarMensaje(this.datosEnviar, socket, packet, this.codificacion);
                break;
            case "xml":
                this.codificacion = op;
                this.datosEnviar = crearDatos(this.medidaTempertatura);
                enviarMensaje(this.datosEnviar, socket, packet, this.codificacion);
                break;
            default:
                enviarMensaje(helpServerA(), socket, packet, this.codificacion);
                break;
        }
    }

    public void manejarPeticion() {

        // Diferenciamos si queremos usar XML o Json
        this.codificacion = establecerCodificacion(this.getDs());
        while (true) {
            // Enviamos los mensajes después de ser creados.
            this.datosEnviar = crearDatos(this.medidaTempertatura);
            enviarMensaje(this.datosEnviar);
            recibirMensaje(this.getDs(), this.codificacion);
        }
    }


    public static void main(String[] args) {
        ServerA sA = new ServerA();
        sA.manejarPeticion();
    }
}
