package App.Server;

import App.Client.Client;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

/*public class ServerB{

    public static final int PORT = 4447;
    public static final int NUM_DATOS = 5;

    private DatagramSocket ds;
    private String datosEnviar;
    private String medidaVelocidad;


    public ServerB() {
        try {
            // Puerto Libre
            this.ds = new DatagramSocket(PORT);
            this.medidaVelocidad = "mtrs";
        } catch (SocketException e) {
            System.err.println("No se ha podido crear el socket.");
        }
    }

    public DatagramSocket getDs() {
        return this.ds;
    }

    List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastList = new ArrayList<>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();
            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }
            networkInterface.getInterfaceAddresses().stream().map(a -> a.getBroadcast()).filter(Objects::nonNull).forEach(broadcastList::add);
        }
        return broadcastList;
    }

    // Arreglar el getClass
    private String crearDatos(String medidaVelocidad) {
        StringBuilder bufferDV = new StringBuilder("DIR VIENTO: [ ");
        StringBuilder bufferPL = new StringBuilder("PROB LLUVIA: [ ");
        StringBuilder bufferVV = new StringBuilder("VEL VIENTO: [ ");

        for (int i = 0; i < NUM_DATOS; i++) {
            bufferDV.append(DirViento.values()[(int) (Math.random() * DirViento.values().length)] + " ");
            bufferPL.append(String.valueOf((int) (Math.random() * 100)) + "% ");

            switch (medidaVelocidad) {
                case "mtrs":
                    if (this.medidaVelocidad.equalsIgnoreCase("mtrs")) {
                        bufferVV.append(String.valueOf((int) (Math.random() * 100)) + "m/s ");
                        break;
                    } else {
                        bufferVV.append(String.valueOf((int) (Math.random() * 100 * 36)) + "km/h ");
                        break;
                    }
                case "klmh":
                    if (this.medidaVelocidad.equalsIgnoreCase("klmh")) {
                        bufferVV.append(String.valueOf((int) (Math.random() * 100 * 36)) + "km/h ");
                        break;
                    } else {
                        bufferVV.append(String.valueOf((int) (Math.random() * 100 / 36)) + "m/s ");
                        break;
                    }
            }


        }
        bufferDV.append("]\n");
        bufferPL.append("]\n");
        bufferVV.append("]\n");

        return this.getClass().getSimpleName() + " ENVIA:\n"
                + "\t " + bufferDV
                + "\t " + bufferPL
                + "\t " + bufferVV
                + "\r\n\r\n";

    }

    public boolean enviarMensaje(String mensaje) {
        try {
            Thread.sleep(2500);
            byte[] buffer = mensaje.getBytes();
            DatagramSocket ds = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(listAllBroadcastAddresses().get(0).getCanonicalHostName()), Client.PORT);
            this.ds.send(packet);
            System.out.print("Datos enviados: " + new String(packet.getData())
                    + "\nPuerto: " + packet.getPort()
                    + "\nAddress: " + packet.getAddress()
                    + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    public String helpServerB() {
        return "Cambiar medida velocidad a km/h [klmh]\n" +
                "Cambiar medida velocidad a ms/s [mtrs]\n" +
                "Salir de los servicios del servidor [EXIT]\n" +
                "\n> ";
    }

    public void manejarPeticion() {
        while (true) {
            // Tenemos un hilo enviando en todo caso
            this.datosEnviar = crearDatos(this.medidaVelocidad);
            enviarMensaje(this.datosEnviar);
            recibirMensaje(this.getDs());

        }
    }

    public boolean recibirMensaje(DatagramSocket socket) {
        try {
            byte[] buffer = new byte[ServerA.MAX_UDP_PACKET_LENGTH];
            //DatagramSocket socket = new DatagramSocket(4446);
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            socket.setSoTimeout(1000);
            socket.receive(packet);
            System.out.println("Recibimos: " + new String(packet.getData()) + " " + packet.getPort() + " " + packet.getAddress());
            tratamientoMensjae(new String(packet.getData()), socket, packet);
        } catch (IOException e) {
            System.err.println("Not data input");
            return false;
        }
        return true;
    }

    public void tratamientoMensjae(String mensaje, DatagramSocket socket, DatagramPacket packet) {

        String op = mensaje.substring(0, 4);
        System.out.println(op);
        switch (op) {
            case "help":
                enviarMensaje(helpServerB(), socket, packet);
                break;
            case "klmh":
                // Creo los datos con las nuevas medidas
                this.medidaVelocidad = op;
                this.datosEnviar = crearDatos(this.medidaVelocidad);
                enviarMensaje(this.datosEnviar, socket, packet);
                break;
            case "mtrs":
                // Creo los datos con las nuevas medidas
                this.medidaVelocidad = op;
                this.datosEnviar = crearDatos(this.medidaVelocidad);
                enviarMensaje(this.datosEnviar, socket, packet);
                break;
            case "exit":
                enviarMensaje("exit", socket, packet);
                break;
            default:
                enviarMensaje(helpServerB(), socket, packet);
                break;
        }


    }


    public static void main(String[] args) {
        ServerB sB = new ServerB();
        sB.manejarPeticion();
    }
}*/
