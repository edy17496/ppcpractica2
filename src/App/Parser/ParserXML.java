package App.Parser;

import App.Server.Server;
import App.Server.ServerA;
import com.google.gson.Gson;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.filter.ElementFilter;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

import org.jdom2.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.beans.XMLEncoder;
import java.io.*;

public class ParserXML {

    // Podemos pasar directamente un String.
    public boolean validateXML(String xml, String xsdPath) {
        try {
            // 1) Creación del objeto que representa el esquema, construcción basada en factoria
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            //  2) Solicitar al esquema un validado
            Schema schema = factory.newSchema(new StreamSource(new StringReader(fileToString(xsdPath))));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(xml)));
        } catch (SAXException | IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String fileToString(String filePath) throws IOException {
        File a = new File(filePath);
        BufferedReader br = new BufferedReader(new FileReader(a));
        String st;
        StringBuffer aux = new StringBuffer();
        while ((st = br.readLine()) != null)
            aux.append(st);
        return aux.toString();
    }


    // Crear XML para datos del servidor A
    public String serializeXMLServerA(String medidaTemperatura) {
        // Flag para medidaTemperatura.
        boolean medidaCelc = false;
        boolean medidaKelv = false;

        // Root Element
        Element datosServerA = new Element("datosServerA");
        Document doc = new Document(datosServerA);

        // DatosServer Elements
        Element humedad = new Element("HUMEDAD");

        Element temperaturaCelcius = new Element("TEMPERATURA_CELCIUS");
        Element temperaturaKelvin = new Element("TEMPERATURA_KELVIN");
        Element temperaturaNoValida = new Element("TEMPERATURA_NO_VALIDA");

        Element pluviometria = new Element("PLUVIOMETRIA");

        for (int i = 1; i <= 10; i++) {
            humedad.addContent(String.valueOf((int) (Math.random() * 100 + 1)) + " ");
            switch (medidaTemperatura) {
                case "celc":
                    temperaturaCelcius.addContent(String.valueOf((int) (Math.random() * 50 + 1)) + " ");
                    medidaCelc = true;
                    break;
                case "kelv":
                    temperaturaKelvin.addContent(String.valueOf((int) (Math.random() * 50 + 1)) + " ");
                    medidaKelv = true;
                    break;
            }
            pluviometria.addContent(String.valueOf((int) (Math.random() * 100 + 1)) + " ");
        }

        doc.getRootElement().addContent(humedad);

        if (medidaCelc)
            doc.getRootElement().addContent(temperaturaCelcius);
        else if (medidaKelv)
            doc.getRootElement().addContent(temperaturaKelvin);
        else {
            temperaturaNoValida.addContent("MEDIDA_NO_VALIDA");
            doc.getRootElement().addContent(temperaturaNoValida);
        }
        doc.getRootElement().addContent(pluviometria);
        return new XMLOutputter().outputString(doc) + "\r\n\r\n";
    }

    // Crear XML para datos del servidor B
    public String serilizeXMLServerB() throws IOException {
        // Root Element
        Element datosServerA = new Element("datosServerB");
        Document doc = new Document(datosServerA);

        // DatosServer Elements
        Element direccionViento = new Element("DIRECCION_VIENTO");
        Element velocidadVientoKmh = new Element("VELOCIDAD_VIENTO_KMH");
        Element probabilidadLluvia = new Element("PROBABILIDAD_LLUVIA");

        for (int i = 1; i <= 10; i++) {
            direccionViento.addContent(String.valueOf(i) + "");
            velocidadVientoKmh.addContent(String.valueOf(i) + " ");
            probabilidadLluvia.addContent(String.valueOf(i) + " ");
        }

        doc.getRootElement().addContent(direccionViento);
        doc.getRootElement().addContent(velocidadVientoKmh);
        doc.getRootElement().addContent(probabilidadLluvia);

        return new XMLOutputter().outputString(doc) + "\r\n\r\n";
    }

    // Crear XML para datos de control
    public String serializeXMLControl(String datosControl) {
        datosControl = datosControl.toUpperCase();
        // Root Element
        Element control = new Element("control");
        Document doc = new Document(control);

        // DatosControl del Servidor
        Element helpServerComando = new Element("HELP_SERVER_COMANDO");
        helpServerComando.addContent(datosControl);

        doc.getRootElement().addContent(helpServerComando);
        return new XMLOutputter().outputString(doc) + "\r\n\r\n";
    }

    // Deserializar los datos de un XML del servidor A
    public String deserializeXMLServerA(String datosXML) {
        return null;
    }


    // Deserializar los datos de un XML del servidor B
    public String deserializeXMLServerB(String datosXML) {
        return null;
    }

    // Crear JSON para datos del servidorA
    public String serializeJSONServerA(String medidaTemperatura) {
        ServerA serverA = new ServerA(medidaTemperatura);
        StringBuilder humedad = new StringBuilder();
        StringBuilder temperatura_celcius;
        StringBuilder temperatura_kelvin;
        StringBuilder pluviometria = new StringBuilder();
        switch (medidaTemperatura) {
            case "celc":
                temperatura_celcius = new StringBuilder();
                for (int i = 1; i <= 10; i++) {
                    humedad.append((int) (Math.random() * 100 + 1)).append(" ");
                    temperatura_celcius.append((int) (Math.random() * 50 + 1)).append(" ");
                    pluviometria.append((int) (Math.random() * 100 + 1)).append(" ");

                }
                serverA.setTemperatura_celcius(temperatura_celcius);
                break;
            case "kelv":
                temperatura_kelvin = new StringBuilder();
                for (int i = 1; i <= 10; i++) {
                    humedad.append((int) (Math.random() * 100 + 1)).append(" ");
                    temperatura_kelvin.append((int) (Math.random() * 50 + 1)).append(" ");
                    pluviometria.append((int) (Math.random() * 100 + 1)).append(" ");
                }
                serverA.setTemperatura_kelvin(temperatura_kelvin);
                break;
        }
        serverA.setHumedad(humedad);
        serverA.setPluviometria(pluviometria);
        Gson gson = new Gson();
        return gson.toJson(serverA);

    }

    // Crear JSON para datos del servidorA
    public String serializeJSONServerB(String medidaTemperatura) {

        return null;
    }

    // Deserializar los datos de un JSON del servidorA
    public String deserializeJSONServerA(String datosJSON) {
        Gson gson = new Gson();
        ServerA serverA = gson.fromJson(datosJSON, ServerA.class);
        return serverA.toString();
    }

    // Deserializar los datos de un JSON del servidorB
    public String deserializeJSONServerB(String datosJSON) {
        return null;
    }

    public String serializeXMLServerAA(String medidaTemperatura) {
        ServerA serverA = new ServerA(medidaTemperatura);
        serverA.setDatos();
        // Root Element
        Element datosServerA = new Element("datosServerA");
        Document doc = new Document(datosServerA);

        // DatosServer Elements
        Element humedad = new Element("HUMEDAD");

        Element temperaturaCelcius = new Element("TEMPERATURA_CELCIUS");
        Element temperaturaKelvin = new Element("TEMPERATURA_KELVIN");
        Element temperaturaNoValida = new Element("TEMPERATURA_NO_VALIDA");

        Element pluviometria = new Element("PLUVIOMETRIA");

        humedad.addContent(serverA.getHumedad().toString());
        switch (medidaTemperatura) {
            case "celc":
                temperaturaCelcius.addContent(serverA.getTemperatura_celcius().toString());
                break;
            case "kelv":
                temperaturaKelvin.addContent(serverA.getTemperatura_kelvin().toString());
                break;
            default:
                temperaturaNoValida.addContent("MEDIDA_NO_VALIDA");
                break;

        }
        pluviometria.addContent(serverA.getPluviometria().toString());

        doc.getRootElement().addContent(humedad);
        if (serverA.getMedidaTempertatura().equalsIgnoreCase("celc"))
            doc.getRootElement().addContent(temperaturaCelcius);
        else if (serverA.getMedidaTempertatura().equalsIgnoreCase("kelv"))
            doc.getRootElement().addContent(temperaturaKelvin);
        else {
            temperaturaNoValida.addContent("MEDIDA_NO_VALIDA");
            doc.getRootElement().addContent(temperaturaNoValida);
        }
        doc.getRootElement().addContent(pluviometria);
        return new XMLOutputter().outputString(doc) + "\r\n\r\n";
    }


    // Obtener comando del XML
    public String getComandoXML(String xml) {
        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            InputStream stream = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            Document document = saxBuilder.build(stream);
            Element element = document.getRootElement();
            ElementFilter filter = new org.jdom2.filter.ElementFilter("HELP_SERVER_COMANDO");
            for (Element c : element.getDescendants(filter)) {
                return c.getTextNormalize();
            }
        } catch (JDOMException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     *                               EJEMPLO DE COMO USAR LAS FUNCIONES                                   *
     */
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        // Genracion y validación de XML Server A
        ParserXML parserXML = new ParserXML();

        System.out.println(parserXML.serializeXMLServerAA("kas"));


    }
}

